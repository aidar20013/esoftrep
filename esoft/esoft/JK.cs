﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace esoft
{
    public partial class JK : Form
    {
        SqlConnection con = new SqlConnection("Data Source=303-11\\SQLSERVER;Initial catalog=esoft;Integrated Security=true;");

        public JK()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            AddNewJK addNewJK = new AddNewJK();
            addNewJK.Show();
        }

        private void JK_Load(object sender, EventArgs e)
        {
            con.Open();

            SqlCommand com = new SqlCommand("select [JK].[Название_ЖК], [JK].[Город], [JK].[Статус_строительства_ЖК], (select COUNT([id_JK]) from [dbo].[houses] where [JK].[id] = [houses].[id_JK]) FROM JK", con);
            SqlDataReader dr = com.ExecuteReader();
            while (dr.Read())
                dataGridView1.Rows.Add(dr[0].ToString(), dr[1].ToString(), dr[2].ToString(), dr[3]);

            dr.Close();
            con.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ModifieJK modifieJK = new ModifieJK();
            int selectedRow = dataGridView1.CurrentRow.Index;
            modifieJK.JKname = dataGridView1[0, selectedRow].Value.ToString();
            modifieJK.Show();
        }

        private void DeleteJK_Click(object sender, EventArgs e)
        {
            int selectedRow = dataGridView1.CurrentRow.Index;
            string JKname = dataGridView1[0, selectedRow].Value.ToString();

            DialogResult dialogResult = MessageBox.Show("Вы действительно хотите удалить эту запись?", "Подтвердите удаление", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                con.Open();

                SqlCommand com = new SqlCommand($"DELETE FROM JK WHERE Название_ЖК = '{JKname}'", con);
                com.ExecuteNonQuery();

                com = new SqlCommand("select [JK].[Название_ЖК], [JK].[Город], [JK].[Статус_строительства_ЖК], (select COUNT([id_JK]) from [dbo].[houses] where [JK].[id] = [houses].[id_JK]) FROM JK", con);
                SqlDataReader dr = com.ExecuteReader();
                dataGridView1.Rows.Clear();
                while (dr.Read())
                    dataGridView1.Rows.Add(dr[0].ToString(), dr[1].ToString(), dr[2].ToString(), dr[3]);

                con.Close();
            }
        }
    }
}
