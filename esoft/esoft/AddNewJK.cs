﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace esoft
{
    public partial class AddNewJK : Form
    {
        SqlConnection con = new SqlConnection("Data Source=303-11\\SQLSERVER;Initial Catalog=esoft;Integrated Security=true");

        public AddNewJK()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            errors.Text = "";
            string name = "";
            string city = "";
            string plan = "";
            int cost = 0;
            int complex = 0;
            
            if (nameText.Text.Length > 0) name = nameText.Text;
            else errors.Text += "Укажите название ЖК\n";
            if (cityText.Text.Length > 0) city = cityText.Text;
            else errors.Text += "Укажите город в котором расположен ЖК\n";

            if (Regex.IsMatch(costText.Text, @"[^\d]"))
                errors.Text += "Затраты на строительство должны быть указаны в виде числа\n";
            else
            {
                if (costText.Text.Length > 0 && Convert.ToInt32(costText.Text) > 0) cost = Convert.ToInt32(costText.Text);
                else errors.Text += "Затраты на строительство должны быть больше 0\n";
            }
            
            if (Regex.IsMatch(complexText.Text, @"[^\d]"))
                errors.Text += "Коэф. добавочной стоимости должен быть указан в виде числа\n";
            else
            {
                if (complexText.Text.Length > 0 && Convert.ToInt32(complexText.Text) > 0) complex = Convert.ToInt32(complexText.Text);
                else errors.Text += "Коэф. добавочной стоимости должен быть больше 0\n";
            }

            foreach (RadioButton rb in groupBox1.Controls)
                if (rb.Checked == true) plan = rb.Text;

            if (errors.Text.Length == 0)
            {
                con.Open();
                SqlCommand com = new SqlCommand($"INSERT INTO JK (Название_ЖК, Затраты_на_строительство_ЖК, Город, Добавочная_стоимость_ЖК, Статус_строительства_ЖК) VALUES ('{name}', '{cost}', '{city}', '{complex}', '{plan}')", con);
                if (com.ExecuteNonQuery() > 0) MessageBox.Show("Запись о жилищном комплексе добавлена.");
                else MessageBox.Show("Возникла ошибка при добавлении.");

                JK jk = (JK)Application.OpenForms["JK"];
                jk.dataGridView1.Rows.Clear();
                com = new SqlCommand("select [JK].[Название_ЖК], [JK].[Город], [JK].[Статус_строительства_ЖК], (select COUNT([id_JK]) from [dbo].[houses] where [JK].[id] = [houses].[id_JK]) FROM JK", con);
                SqlDataReader dr = com.ExecuteReader();
                jk.dataGridView1.Rows.Clear();
                while (dr.Read())
                    jk.dataGridView1.Rows.Add(dr[0].ToString(), dr[1].ToString(), dr[2].ToString(), dr[3]);
                con.Close();
            }
        }
    }
}
