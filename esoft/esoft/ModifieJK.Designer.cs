﻿namespace esoft
{
    partial class ModifieJK
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ModifieJK));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.errors = new System.Windows.Forms.Label();
            this.complexText = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.statusSell = new System.Windows.Forms.RadioButton();
            this.statusBuild = new System.Windows.Forms.RadioButton();
            this.statusPlan = new System.Windows.Forms.RadioButton();
            this.cityText = new System.Windows.Forms.TextBox();
            this.costText = new System.Windows.Forms.TextBox();
            this.nameText = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(313, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(138, 50);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 17;
            this.pictureBox1.TabStop = false;
            // 
            // errors
            // 
            this.errors.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.errors.Location = new System.Drawing.Point(63, 261);
            this.errors.Name = "errors";
            this.errors.Padding = new System.Windows.Forms.Padding(8);
            this.errors.Size = new System.Drawing.Size(356, 95);
            this.errors.TabIndex = 27;
            // 
            // complexText
            // 
            this.complexText.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.complexText.Location = new System.Drawing.Point(442, 203);
            this.complexText.Name = "complexText";
            this.complexText.Size = new System.Drawing.Size(234, 22);
            this.complexText.TabIndex = 26;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label4.Location = new System.Drawing.Point(351, 167);
            this.label4.Name = "label4";
            this.label4.Padding = new System.Windows.Forms.Padding(8);
            this.label4.Size = new System.Drawing.Size(57, 30);
            this.label4.TabIndex = 25;
            this.label4.Text = "Город";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label3.Location = new System.Drawing.Point(101, 139);
            this.label3.Name = "label3";
            this.label3.Padding = new System.Windows.Forms.Padding(8);
            this.label3.Size = new System.Drawing.Size(307, 30);
            this.label3.TabIndex = 24;
            this.label3.Text = "Затраты на строительство жилищного комплекса";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label2.Location = new System.Drawing.Point(63, 195);
            this.label2.Name = "label2";
            this.label2.Padding = new System.Windows.Forms.Padding(8);
            this.label2.Size = new System.Drawing.Size(345, 30);
            this.label2.TabIndex = 23;
            this.label2.Text = "Коэффициент добавочной стоимости на строительство";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label1.Location = new System.Drawing.Point(310, 111);
            this.label1.Name = "label1";
            this.label1.Padding = new System.Windows.Forms.Padding(8);
            this.label1.Size = new System.Drawing.Size(98, 30);
            this.label1.TabIndex = 22;
            this.label1.Text = "Название ЖК";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.statusSell);
            this.groupBox1.Controls.Add(this.statusBuild);
            this.groupBox1.Controls.Add(this.statusPlan);
            this.groupBox1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.groupBox1.Location = new System.Drawing.Point(476, 256);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(8);
            this.groupBox1.Size = new System.Drawing.Size(200, 100);
            this.groupBox1.TabIndex = 21;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Статус";
            // 
            // statusSell
            // 
            this.statusSell.AutoSize = true;
            this.statusSell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.statusSell.Location = new System.Drawing.Point(11, 70);
            this.statusSell.Name = "statusSell";
            this.statusSell.Size = new System.Drawing.Size(57, 18);
            this.statusSell.TabIndex = 2;
            this.statusSell.TabStop = true;
            this.statusSell.Text = "selling";
            this.statusSell.UseVisualStyleBackColor = true;
            // 
            // statusBuild
            // 
            this.statusBuild.AutoSize = true;
            this.statusBuild.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.statusBuild.Location = new System.Drawing.Point(11, 47);
            this.statusBuild.Name = "statusBuild";
            this.statusBuild.Size = new System.Drawing.Size(48, 18);
            this.statusBuild.TabIndex = 1;
            this.statusBuild.TabStop = true;
            this.statusBuild.Text = "built";
            this.statusBuild.UseVisualStyleBackColor = true;
            // 
            // statusPlan
            // 
            this.statusPlan.AutoSize = true;
            this.statusPlan.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.statusPlan.Location = new System.Drawing.Point(11, 24);
            this.statusPlan.Name = "statusPlan";
            this.statusPlan.Size = new System.Drawing.Size(47, 18);
            this.statusPlan.TabIndex = 0;
            this.statusPlan.TabStop = true;
            this.statusPlan.Text = "plan";
            this.statusPlan.UseVisualStyleBackColor = true;
            // 
            // cityText
            // 
            this.cityText.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.cityText.Location = new System.Drawing.Point(442, 175);
            this.cityText.Name = "cityText";
            this.cityText.Size = new System.Drawing.Size(234, 22);
            this.cityText.TabIndex = 20;
            // 
            // costText
            // 
            this.costText.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.costText.Location = new System.Drawing.Point(442, 147);
            this.costText.Name = "costText";
            this.costText.Size = new System.Drawing.Size(234, 22);
            this.costText.TabIndex = 19;
            // 
            // nameText
            // 
            this.nameText.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.nameText.Location = new System.Drawing.Point(442, 119);
            this.nameText.Name = "nameText";
            this.nameText.Size = new System.Drawing.Size(234, 22);
            this.nameText.TabIndex = 18;
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.button2.Location = new System.Drawing.Point(572, 432);
            this.button2.Name = "button2";
            this.button2.Padding = new System.Windows.Forms.Padding(8);
            this.button2.Size = new System.Drawing.Size(104, 42);
            this.button2.TabIndex = 29;
            this.button2.Text = "Закрыть";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.button1.Location = new System.Drawing.Point(66, 432);
            this.button1.Name = "button1";
            this.button1.Padding = new System.Windows.Forms.Padding(8);
            this.button1.Size = new System.Drawing.Size(104, 42);
            this.button1.TabIndex = 28;
            this.button1.Text = "Изменить";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // ModifieJK
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(735, 495);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.errors);
            this.Controls.Add(this.complexText);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.cityText);
            this.Controls.Add(this.costText);
            this.Controls.Add(this.nameText);
            this.Controls.Add(this.pictureBox1);
            this.Name = "ModifieJK";
            this.Text = "Редактирование ЖК";
            this.Load += new System.EventHandler(this.ModifieJK_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label errors;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox complexText;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton statusSell;
        private System.Windows.Forms.RadioButton statusBuild;
        private System.Windows.Forms.RadioButton statusPlan;
        private System.Windows.Forms.TextBox cityText;
        private System.Windows.Forms.TextBox costText;
        private System.Windows.Forms.TextBox nameText;
    }
}